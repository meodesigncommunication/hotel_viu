<?php

/* @Page:/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/hotel_viu/landing_page/user/plugins/error/pages */
class __TwigTemplate_7e54fa4a20b7efb220f806bcc50d3096d000ad79f6a50108f9cb1af4b5ca6a39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("PLUGIN_ERROR.ERROR_MESSAGE");
        echo "

";
    }

    public function getTemplateName()
    {
        return "@Page:/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/hotel_viu/landing_page/user/plugins/error/pages";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ 'PLUGIN_ERROR.ERROR_MESSAGE'|t }}

", "@Page:/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/hotel_viu/landing_page/user/plugins/error/pages", "");
    }
}
