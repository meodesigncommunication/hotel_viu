<?php

/* modular/showcase.html.twig */
class __TwigTemplate_f0c6ab53f9fe5f7a1bcdb83b45e75204133a63b6014eda8e2df6cf8f7de233a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"showcase\" style=\"background-image: url('";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/swiss_mountain.jpg");
        echo "');\">
    <div class=\"wrapper\">
        <p>";
        // line 3
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->translate("LANDING_PAGE.OPEN_SEASON");
        echo "</p>
        <a class=\"logo fade-animation\" href=\"#\" title=\"Logo Hotel VIU Villars\">
            <img src=\"";
        // line 5
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/logo-viu.svg");
        echo "\" alt=\"logo viu\">
            <span>Hotel Villars</span>
        </a>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "modular/showcase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 5,  25 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"showcase\" style=\"background-image: url('{{ url('theme://images/swiss_mountain.jpg') }}');\">
    <div class=\"wrapper\">
        <p>{{ 'LANDING_PAGE.OPEN_SEASON'|t }}</p>
        <a class=\"logo fade-animation\" href=\"#\" title=\"Logo Hotel VIU Villars\">
            <img src=\"{{ url('theme://images/logo-viu.svg') }}\" alt=\"logo viu\">
            <span>Hotel Villars</span>
        </a>
    </div>
</section>", "modular/showcase.html.twig", "/Users/kylemobilia/Documents/_WORK/MEO/hotel_viu/landing_page/user/themes/hotelviu/templates/modular/showcase.html.twig");
    }
}
