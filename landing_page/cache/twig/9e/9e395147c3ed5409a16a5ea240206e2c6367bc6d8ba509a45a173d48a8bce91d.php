<?php

/* partials/layout.html.twig */
class __TwigTemplate_4c69339904b94694ba7502c4f3eddc6e2804dad8d048018b402eaabedaa2c5a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
            'footer_javascript' => array($this, 'block_footer_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", array()), "getActive", array())) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", array()), "site", array()), "default_lang", array())));
        echo "\">
<head>
    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 28
        echo "</head>
<body>

<nav class=\"languages\">
    <div class=\"mobile-nav\"> <div></div> <div></div> <div></div> </div>
    ";
        // line 33
        $this->loadTemplate("partials/langswitcher.html.twig", "partials/layout.html.twig", 33)->display($context);
        // line 34
        echo "</nav>

";
        // line 36
        $this->displayBlock('content', $context, $blocks);
        // line 38
        echo "
";
        // line 39
        $this->displayBlock('footer_javascript', $context, $blocks);
        // line 42
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "
</body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "        <meta charset=\"utf-8\" />
        <title>";
        // line 6
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "metadata", array()), "title", array());
        echo "</title>

        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">

        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        <meta name=\"description\" content=\"";
        // line 12
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "metadata", array()), "description", array());
        echo "\">

        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 14
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/favicon.png");
        echo "\" />

        ";
        // line 16
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "        ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", array(), "method");
        echo "

        ";
        // line 21
        $this->displayBlock('javascripts', $context, $blocks);
        // line 25
        echo "        ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", array(), "method");
        echo "

    ";
    }

    // line 16
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 17
        echo "            ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", array(0 => "theme://css-compiled/style.css"), "method");
        // line 18
        echo "        ";
    }

    // line 21
    public function block_javascripts($context, array $blocks = array())
    {
        // line 22
        echo "            ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "jquery", 1 => 101), "method");
        // line 23
        echo "            ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/jquery.fancybox.min.js"), "method");
        // line 24
        echo "        ";
    }

    // line 36
    public function block_content($context, array $blocks = array())
    {
    }

    // line 39
    public function block_footer_javascript($context, array $blocks = array())
    {
        // line 40
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", array(0 => "theme://js/mobile-nav.js"), "method");
    }

    public function getTemplateName()
    {
        return "partials/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 40,  132 => 39,  127 => 36,  123 => 24,  120 => 23,  117 => 22,  114 => 21,  110 => 18,  107 => 17,  104 => 16,  96 => 25,  94 => 21,  88 => 19,  86 => 16,  81 => 14,  76 => 12,  67 => 6,  64 => 5,  61 => 4,  54 => 42,  52 => 39,  49 => 38,  47 => 36,  43 => 34,  41 => 33,  34 => 28,  32 => 4,  27 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ grav.language.getActive ?: grav.config.site.default_lang }}\">
<head>
    {% block head %}
        <meta charset=\"utf-8\" />
        <title>{{ page.header.metadata.title}}</title>

        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">

        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        <meta name=\"description\" content=\"{{ page.header.metadata.description }}\">

        <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/favicon.png') }}\" />

        {% block stylesheets %}
            {% do assets.addCss('theme://css-compiled/style.css') %}
        {% endblock %}
        {{ assets.css() }}

        {% block javascripts %}
            {% do assets.addJs('jquery', 101) %}
            {% do assets.addJs('theme://js/jquery.fancybox.min.js') %}
        {% endblock %}
        {{ assets.js() }}

    {% endblock head %}
</head>
<body>

<nav class=\"languages\">
    <div class=\"mobile-nav\"> <div></div> <div></div> <div></div> </div>
    {% include 'partials/langswitcher.html.twig' %}
</nav>

{% block content %}
{% endblock %}

{% block footer_javascript %}
    {% do assets.addJs('theme://js/mobile-nav.js') %}
{% endblock %}
{{ assets.js() }}
</body>
</html>", "partials/layout.html.twig", "/Users/kylemobilia/Documents/_WORK/MEO/hotel_viu/landing_page/user/themes/hotelviu/templates/partials/layout.html.twig");
    }
}
