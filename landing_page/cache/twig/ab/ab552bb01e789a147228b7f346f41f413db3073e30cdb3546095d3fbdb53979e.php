<?php

/* modular/text_images.html.twig */
class __TwigTemplate_38d3fedf06a3b493aa0d549fffb2b56237b5f48505472d082389ba18f42b2324 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"text-images\">
    <div class=\"global-wrapper\">
        <div class=\"wrapper\">
            <div>
                <h1>";
        // line 5
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
        echo "</h1>
                ";
        // line 6
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
            </div>
        </div>
        <div class=\"wrapper\">
            <div>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"";
        // line 11
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_1", array()), array(), "array"), "url", array());
        echo "\">
                    <div class=\"image-gallery\" style=\"background-image: url('";
        // line 12
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_1", array()), array(), "array"), "url", array());
        echo "')\">
                        <img src=\"";
        // line 13
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/square.png");
        echo "\" />
                    </div>
                </a>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"";
        // line 16
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_2", array()), array(), "array"), "url", array());
        echo "\">
                    <div class=\"image-gallery\" style=\"background-image: url('";
        // line 17
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_2", array()), array(), "array"), "url", array());
        echo "')\">
                        <img src=\"";
        // line 18
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/square.png");
        echo "\" />
                    </div>
                </a>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"";
        // line 21
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_3", array()), array(), "array"), "url", array());
        echo "\">
                    <div class=\"image-gallery\" style=\"background-image: url('";
        // line 22
        echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", array()), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "image_3", array()), array(), "array"), "url", array());
        echo "')\">
                        <img src=\"";
        // line 23
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://images/rectangle.png");
        echo "\" />
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "modular/text_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 23,  69 => 22,  65 => 21,  59 => 18,  55 => 17,  51 => 16,  45 => 13,  41 => 12,  37 => 11,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"text-images\">
    <div class=\"global-wrapper\">
        <div class=\"wrapper\">
            <div>
                <h1>{{ page.header.title }}</h1>
                {{ page.content }}
            </div>
        </div>
        <div class=\"wrapper\">
            <div>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"{{ page.media[page.header.image_1].url }}\">
                    <div class=\"image-gallery\" style=\"background-image: url('{{ page.media[page.header.image_1].url }}')\">
                        <img src=\"{{ url('theme://images/square.png') }}\" />
                    </div>
                </a>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"{{ page.media[page.header.image_2].url }}\">
                    <div class=\"image-gallery\" style=\"background-image: url('{{ page.media[page.header.image_2].url }}')\">
                        <img src=\"{{ url('theme://images/square.png') }}\" />
                    </div>
                </a>
                <a class=\"gallery\" data-fancybox=\"gallery\" href=\"{{ page.media[page.header.image_3].url }}\">
                    <div class=\"image-gallery\" style=\"background-image: url('{{ page.media[page.header.image_3].url }}')\">
                        <img src=\"{{ url('theme://images/rectangle.png') }}\" />
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>", "modular/text_images.html.twig", "/Users/kylemobilia/Documents/_WORK/MEO/hotel_viu/landing_page/user/themes/hotelviu/templates/modular/text_images.html.twig");
    }
}
