<?php

/* modular/text_form.html.twig */
class __TwigTemplate_63ee54227b6ad771a2bdad3bddc8c59a4fea350d35a3114117ed8aefd0ce01a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"text-form\">
    <div class=\"global-wrapper\">
        <div class=\"wrapper\">
            <div class=\"content\">
                <h1>";
        // line 5
        echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "title", array());
        echo "</h1>
                ";
        // line 6
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "
            </div>
            <div class=\"form\">
                ";
        // line 9
        $this->loadTemplate("forms/form.html.twig", "modular/text_form.html.twig", 9)->display($context);
        // line 10
        echo "            </div>
        </div>
        <div class=\"wrapper\">
            <!-- EMPTY COL -->
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "modular/text_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 10,  35 => 9,  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"text-form\">
    <div class=\"global-wrapper\">
        <div class=\"wrapper\">
            <div class=\"content\">
                <h1>{{ page.header.title }}</h1>
                {{ page.content }}
            </div>
            <div class=\"form\">
                {% include \"forms/form.html.twig\" %}
            </div>
        </div>
        <div class=\"wrapper\">
            <!-- EMPTY COL -->
        </div>
    </div>
</section>", "modular/text_form.html.twig", "/Users/kylemobilia/Documents/MEO_WEBSITE_BITBUCKET/hotel_viu/landing_page/user/themes/hotelviu/templates/modular/text_form.html.twig");
    }
}
