<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://hotelviu/hotelviu.yaml',
    'modified' => 1545388902,
    'data' => [
        'enabled' => true,
        'production-mode' => true,
        'grid-size' => 'grid-lg',
        'header-fixed' => true,
        'header-animated' => true,
        'header-dark' => false,
        'header-transparent' => false,
        'sticky-footer' => true,
        'blog-page' => '/blog',
        'content' => [
            'items' => '@self.children',
            'order' => [
                'by' => 'date',
                'dir' => 'desc'
            ],
            'limit' => 10,
            'pagination' => true
        ],
        'spectre' => [
            'exp' => false,
            'icons' => false
        ]
    ]
];
