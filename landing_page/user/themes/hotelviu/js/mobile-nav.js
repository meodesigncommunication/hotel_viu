$(function(){

    textFormElement();
    textImagesElement();

    $(window).scroll(function(){
        textFormElement();
        textImagesElement();
    });

    $('.mobile-nav').click(function(){
        if($(this).parent().hasClass('open')) {
            $(this).parent().removeClass('open');
        }else{
            $(this).parent().addClass('open');
        }
    });

    function textImagesElement() {
        $currentScroll = $(window).scrollTop();
        $('.text-images').each(function(){
            if($(this).scrollTop() <= $currentScroll-150) {
                $(this).addClass('fade-in-element');
            }else{
                $(this).removeClass('fade-in-element');
            }
        });
    }

    function textFormElement() {
        $currentScroll = $(window).scrollTop();
        $('.text-form').each(function(){
            if($(this).scrollTop() <= $currentScroll-50) {
                $(this).addClass('fade-in-element');
            }else{
                $(this).removeClass('fade-in-element');
            }
        });
    }

});