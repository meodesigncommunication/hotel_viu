---
title: 'Un nouveau visage pour vous accueillir'
media_order: 'room_03.jpg,room_01.jpg,room_02.jpg,room_04.jpg'
image_1: room_02.jpg
image_2: room_03.jpg
image_3: room_04.jpg
---

La promesse L’Hôtel VIU : vous faire vivre un séjour idyllique au travers d’une expérience unique. 

Après une journée de ski, de randonnée, de VTT ou de visites à Villars-sur-Ollon et sa région, vous pourrez vous détendre dans le cadre moderne, confortable et chaleureux de notre établissement qui ouvrira ses portes en hiver 2019. Vous y trouverez un cocon douillet où vous serez choyé face à un panorama intemporel qui ne vous laissera d’autres choix que de façonner des moments inoubliables qui se graveront dans vos plus agréables souvenirs ; vous vous approprierez sans nul doute ces précieux instants et oublierez, le temps d’une parenthèse, votre quotidien.
