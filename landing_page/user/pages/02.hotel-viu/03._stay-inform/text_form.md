---
title: 'Restez informé'
form:
    name: contact-form
    action: /
    fields:
        -
            name: email
            placeholder: 'Email'
            type: email
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Envoyer
    process:
        -
            email:
                from: 'noreply@hotelviu.ch'
                to:
                    - info@hotelviu.ch
                    - kyle.mobilia@meomeo.ch
                subject: 'Hôtel VIU | Je souhaite rester informer'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: newsletter-register-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Merci, nous vous tiendrons informé'
---

Si vous désirez recevoir des informations et des offres de l’Hôtel VIU, inscrivez votre e-mail dans le champ ci-dessous